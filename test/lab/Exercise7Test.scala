package lab

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List.Nil
import u03.Lists.List.Cons
import u03.Lists.List
import u03.Streams.Stream

class Exercise7Test {

  val  fibs: Stream[Int] = lab.Exercise7.fibs()

  @Test def fibsTest() {
    assertEquals(
      Cons(0,Cons(1,Cons(1,Cons(2,Cons(3,Cons(5,Cons(8,Cons(13,Nil())))))))),
      Stream.toList(Stream.take(fibs)(8))
    )
  }

}
