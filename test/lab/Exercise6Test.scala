package lab

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List.Nil
import u03.Lists.List.Cons
import u03.Lists.List
import u03.Streams.Stream

import lab.Exercise6.constant

class Exercise6Test {

  @Test def testStreamDrop() {
    assertEquals(
      Cons("x",Cons("x",Cons("x",Cons("x",Cons("x",Nil()))))),
      Stream.toList(Stream.take(constant("x"))(5))
    )
  }

}
