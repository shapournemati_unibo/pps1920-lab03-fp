package lab

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules.Person
import u02.Modules.Person.{Student, Teacher}
import u03.Lists.List.Nil
import u03.Lists.List.Cons
import u03.Lists.List

class exercise3Test {

  val people: List[Person] = Cons(Student("A", 1), Cons(Teacher("B", "PCD"), Cons(Student("C", 2), Cons(Teacher("D", "PPS"), Nil()))))

  @Test def twoCoursesTest() {
    val expected = Cons("PCD", Cons("PPS", Nil()))
    assertEquals(expected, lab.Exercise3.getCoursesOfTeachers(people))
  }

}
