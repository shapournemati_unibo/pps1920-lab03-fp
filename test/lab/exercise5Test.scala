package lab

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List.Nil
import u03.Lists.List.Cons
import u03.Lists.List
import u03.Streams.Stream

class exercise5Test {

  val s = Stream.take(Stream.iterate (0)(_+1))(10)

  @Test def testStreamDrop() {
    assertEquals(
      Cons(6,Cons(7,Cons(8,Cons(9,Nil())))),
      Stream.toList(Stream.drop(s)(6))
    )
  }


}
