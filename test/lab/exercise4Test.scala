package lab

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u03.Lists.List.Nil
import u03.Lists.List.Cons
import u03.Lists.List

class exercise4Test {

  val lst = Cons(3,Cons(7,Cons(1,Cons(5, Nil()))))

  @Test def testFoldLeft() {
    assertEquals(-16,lab.Exercise4.foldLeft(lst)(0)(_-_))
  }

  @Test def testFoldRight(): Unit = {
    assertEquals(-8,lab.Exercise4.foldRight(lst)(0)(_-_))
  }


}
