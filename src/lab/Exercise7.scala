package lab

import u03.Streams.Stream
import u03.Streams.Stream.cons

object Exercise7 {

  def fibs(): Stream[Int] = {
    Stream.map(Stream.iterate(0)(_ + 1))(fib(_)) 
  }

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case n if (n>= 2) => fib(n - 1) + fib(n - 2)
    case _ => -1 //?
  }

}
