package lab

import u02.Modules.Person
import u02.Modules.Person.Teacher
import u03.Lists.List
import u03.Lists.List.{Cons, filter, flatMap, map}
import u03.Lists.List.Nil

object Exercise3 extends App {

  def getCoursesOfTeachers(people: List[Person]): List[String] = {
    flatMap(people)(person => person match {
      case Teacher(_, course) => Cons(course, Nil())
      case _ => Nil()
    })

  }

}
