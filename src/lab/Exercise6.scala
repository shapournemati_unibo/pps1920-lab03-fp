package lab
import u03.Streams.Stream
import u03.Streams.Stream.cons


object Exercise6 {

  def constant[A](element: A): Stream[A] = {
    cons(element,constant(element))
  }

}
