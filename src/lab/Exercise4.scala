package lab

import u03.Lists.List
import u03.Lists.List.{Cons, Nil}

object Exercise4 extends {

  def foldLeft[A](l: List[A])(initialValue: A)(operator: (A,A)=>A): A = l match {
    case Cons(h,t) => operator(foldLeft(t)(initialValue)(operator), h)
    case Nil() => initialValue
  }

  def foldRight[A](l: List[A])(initialValue: A)(operator: (A,A)=>A): A = l match {
    case Cons(h,t) => operator(h,foldRight(t)(initialValue)(operator))
    case Nil() => initialValue
  }

}
