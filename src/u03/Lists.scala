package u03

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    /*def map[A, B](l: List[A])(mapper: A => B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match {
      case Cons(h, t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()
    }*/

    def drop[A](l: List[A], n: Int): List[A] = (l, n) match {
      case (Cons(_, t), n) if (n > 0) => drop(t, n - 1)
      case _ => l
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case Nil() => Nil()
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = flatMap(l)(e => Cons(mapper(e), Nil()))

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = flatMap(l1)(e => pred(e) match {
      case true => Cons(e, Nil())
      case _ => Nil()
    })

    def max(l: List[Int]): Option[Int] = l match {
      case Cons(h, Cons(h2, t)) => max(Cons(Math.max(h,h2),t))
      case Cons(h, Nil()) => Some(h)
      case Nil() => None
    }

  }

}

object ListsMain extends App {

  import Lists._

  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  //println(List.sum(l)) // 60

  import List._
  import u03.Lists.List
  //println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30

  val lst = Cons(10, Cons(20, Cons(30, Nil())))
  println(drop(lst, 1), // Cons(20,Cons(30, Nil()))
    drop(lst, 2), // Cons(30, Nil())
    drop(lst, 5)) // Nil()*/

  /*val  lst = Cons(10, Cons(20, Cons(30, Nil())))
  println(flatMap(lst)(v => Cons(v+1, Nil())) // Cons(11,Cons(21,Cons(31,Nil())))
  ,flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))// Cons(11,Cons(12,Cons(21,Cons(22,Cons(31,Cons(32,Nil()))))))*/

  println(map(lst)(e => e * 2))

  println(max(Cons(10, Cons(25, Cons(20, Nil())))) // Some (25)
  ,max(Nil())) // None())


}